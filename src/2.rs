fn main() {
    let mut hello = String::from("First");
    println!("{}",pig_latin(&mut hello));
}

fn pig_latin(string: &mut String) -> String {
    let li = &string[0..1];
    let ay = "ay";
    match li {
        "a" => string.push_str("-hay"),
        "e" => string.push_str("-hay"),
        "i" => string.push_str("-hay"),
        "o" => string.push_str("-hay"),
        "u" => string.push_str("-hay"),
        "y" => string.push_str("-hay"),
        "A" => string.push_str("-hay"),
        "E" => string.push_str("-hay"),
        "I" => string.push_str("-hay"),
        "O" => string.push_str("-hay"),
        "U" => string.push_str("-hay"),
        "Y" => string.push_str("-hay"),
        _ => {
            let together = format!("-{}{}",li, ay);
            string.push_str(together.as_str());
            string.remove(0);
        }
    };
    
    return string.to_string();
}