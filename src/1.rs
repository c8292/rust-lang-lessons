use std::collections::HashMap;

fn main() {
    let mut vect = vec![1,5,5,9,8,2,4,10];
    println!("Среднее арифметическое массива: {}", foo1(&vect));
    println!("Средний элемент массива: {}", foo2(&mut vect));
    println!("Количество элементов массива: {:#?}", foo3(&vect));
}

 fn foo1(list: &Vec<i32>) -> f32 {
    let mut counter: f32 = 0.0;
    for i in list {
        counter += *i as f32;
    }
    counter = counter / list.len() as f32;
    return counter;
}

fn foo2(list: &mut Vec<i32>) -> i32 {
    list.sort();
    let counter = list.len() as i32 / 2;
    return list[counter as usize];
}

fn foo3(keys: &Vec<i32>) -> HashMap<i32, i32> {
    let mut values = Vec::new();
    let mut i = 0;
    while i <= keys.len() {
        values.push(0);
        i += 1;
    }

    
    let counter: HashMap<_, _> = keys.into_iter().zip(values.into_iter()).collect();
    let mut counter2 = HashMap::new();

    for (key, _value) in &counter {
        let mut count = 0;
        for i in keys.into_iter() {
            if *i == **key {
                count += 1;
                counter2.insert(**key, count);
            }
        }
    }

    return counter2;
}

