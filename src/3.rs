use std::io::stdin;
use std::collections::HashMap;

fn main() {
    let mut fknslaves: HashMap<String, String> = HashMap::new();
    //------------------------------------------------------------
    println!("Сейчас мы будем добавлять сотрудников в отделы ;)");
    //------------------------------------------------------------
    loop {
        let mut exit = String::new();
        let mut depname = String::new();
        let mut employee = String::new();

        println!("Хочешь выйти?");
        stdin().read_line(&mut exit).expect("Не удалось прочитать");
        if &exit[0..4] == "да" || &exit[0..4] == "Да" {
            break;
        }
        else {
            println!("-------------------------------------");
            println!("Значит не выходим");
            println!("-------------------------------------");

            let mut answer = String::new();
            println!("Хочешь добавить пользователя в отдел?");
            stdin().read_line(&mut answer).expect("Не удалось прочитать");

            println!("-------------------------------------");

            if &answer[0..4] == "Да" || &answer[0..4] == "да" {

                println!("Название отдела:");
                stdin().read_line(&mut depname).expect("Не удалось прочитать");

                println!("----------------");

                println!("Имя сотрудника:");
                stdin().read_line(&mut employee).expect("Не удалось прочитать");

                println!("----------------");
			    fknslaves.insert(depname, employee);
            }
            println!("Отдел и сотрудник: {:#?}", fknslaves);
        }
    }
}

